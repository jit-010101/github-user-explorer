# GitHub User Explorer

> _"I couldn't come up with another name, so here we are."_

**This is a Monorepo** managed by **[Yarn 2 Workspaces](https://yarnpkg.com/features/workspaces)** and **[NX](https://nx.dev/)** consisting of a **Client (=Next.js App)** and a **Server (=Express.js Proxy)**.

The Express.js Server is used for Caching and Proxying the requests to the official GitHub API. 

The Client can theoretically run detached from the Server, and possibly be hosted elsewhere (if we implement CORS and Authentification f.e.). 
_ _ _ _ _ _ _ _ _ _
### Takeaway from this project:

I did this project in a way to get an idea about the state of technologies I haven't used yet (or for a while). I'm usually a fan of minimal dependencies that fullfill a very specific role and are architecturally very clean (less risk of breaking features from major to major) and rather implement most of what I need from scratch in an abstraction layer (e.g. observables, a state machine, router, or the utility-classes e.g.) but that wouldn't be feasible in that time here.

Bottomline React maybe with Redux and Express.js, maybe not even in an Monorepo would be enought for many cases. But maybe even less - and React is already to opinionated (when it comes to tying yourself to a specific toolset especially for ensuring deployment and code-quality, e.g. linting).

In regards to "opinionated" NX might be a good concept in terms of architecture that offers you enought freedom saving you time for development, but that needs to be evaluated in the next project on its own.

The last 20% for optimization by frameworks and tools like Next.js and Webpack introduce a big maintenance overhead for development if things go wrong (which require deep understanding of the "self-managing" architecture behind it, which is counterproductive to their aim of keeping things simple, driving anyone to a wall of maintenance mess introducing possible big side-effects no matter your coverage), especially as they move on and introduce breaking bugs e.g. via rolling release dependencies that they don't care about (see package.json - resolution, for Next.js, which is also why I would always only use Yarn and not NPM for any project unless they introduce a similiar way of handling deep troublesome dependencies). That said - Next.js does has some positive aspects to offer as well (especially in regards to error-handling) even for small projects, if you don't believe the general consensus on the public web and concentrate on what you need - so its not black and white - rather grey.

**More specifically:**

1. **Quality projects take time** (e.g. core concepts, data modelling, UX prototypes), ... 
2. ...and **a lot of colaborative, positive energy** (iterative process, colaborative idea-collection, feedback rounds).
3. **[Figma](https://www.figma.com)** is quite nice for colaboration, not just UX-Prototypes
4. **Yarn 2 Workspaces with Monorepos** are really efficient if implemented right (unsure about Deployment tought)
5. But you have to be carefull about dependencies (including heavily popular ones)
6. Using NX from the start would've saved me a lot of time (maybe)
7. I still don't like Webpack, or the code err ... mess it produces even if it has become better (even if it's abstracted away by Next.js)
8. **In regards to Packages / Dependencies:**
   - **Good** (does fill a specific role): 
        - NX (albeit a bit bugged, e.g. saving it's cache in node_modules despite being used with Yarn 2)
        - Yarn 2
        - Express.js
   - **Good** in the right cirumstances: 
        - Next.js
        - React
        - Framer-Motion (awesome animations for any App written with React)
        - TailwindCSS (if you have a specific UX you have to implement)
   - **Unsure**/needs further evaluation:
        - [atlasian/changesets](https://github.com/atlassian/changesets)
        - [bit](http://www.bit.dev)
   - **Needs Maintenance**/Alternatives welcome:
        - ESLint (has troubles in Monorepos/with Yarn 2)
   - **Unmaintaned**/Not recommended/Broken:
        - Lerna
        - TSLint (RIP)

_ _ _ _ _ _ _ _ _ _
## IDE & ENV:
This was created and is maintained with **_[VSCode (recommended)](https://code.visualstudio.com/)_**\
It was tested on Node 14.16.1 LTS - managed by **[nvs](https://github.com/jasongin/nvs)**
_ _ _ _ _ _ _ _ _ _

## STRUCTURE:
| Folder |Part |
| ------ | ------ |
| `packages/client/src` | <= **Client** |
| `packages/server/src` | <= **Server** |
_ _ _ _ _ _ _ _ _ _
## USAGE:
1. `yarn install`
2. `yarn build`
3. `yarn start`
_ _ _ _ _ _ _ _ _ _
## DEV-TASKS:
- `yarn dev`
- `yarn server-dev`
- `yarn client-dev`
- `yarn lint`
- `yarn test`
_ _ _ _ _ _ _ _ _ _
## TECH-STACK (BASELINE):
#### CLIENT:
    * Next.js
    * React (with react-query)
    * Material UI
    * Framer-Motion
    * TypeScript
#### SERVER:
    * Express.js
    * TypeScript
_ _ _ _ _ _ _ _ _ _
## LINKS:
* https://github.com/jcalderonzumba/nextjs-express-ts-monorepo
* https://nx.dev/latest/react/guides/nextjs
* https://guess-js.github.io/

