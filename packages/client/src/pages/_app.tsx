import React from "react";

import type { AppProps } from 'next/app'

import { QueryCache, QueryClient, QueryClientProvider } from "react-query";
import { ReactQueryDevtools } from 'react-query/devtools'
import { RecoilRoot } from "recoil";
import { AnimateSharedLayout } from "framer-motion";

const queryCache:QueryCache = new QueryCache({});
const queryClient:QueryClient = new QueryClient({queryCache});

function GitHubUserExplorer({ Component, pageProps }: AppProps) {
  return (
  <RecoilRoot>
    <QueryClientProvider client={queryClient}>
      <AnimateSharedLayout>
        <Component {...pageProps} />
      </AnimateSharedLayout>
      <ReactQueryDevtools initialIsOpen={false} />
    </QueryClientProvider>
  </RecoilRoot>
  )
}

export default GitHubUserExplorer;