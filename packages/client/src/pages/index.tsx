import React from "react"; 

import ApplicationContainer from '../components/ApplicationContainer';

const StartPage: React.FC = () => {
    return(
        <ApplicationContainer>
            This is just an empty page so far
        </ApplicationContainer>
    );
}

export default StartPage;