import React from 'react';

import PropTypes from 'prop-types';

import { ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';

import { DefaultTheme } from "./UIThemes";

import TopBar from "./TopBar";
import ContentArea from "./ContentArea";

import UserView from './UserView';

const ApplicationContainer: React.FC = ({ children }) => {
    return (
        <React.Fragment>
            <ThemeProvider theme={DefaultTheme}>
                <CssBaseline />
                <TopBar></TopBar>
                <ContentArea>
                    <UserView></UserView>
                </ContentArea>
                <div></div>
            </ThemeProvider>
        </React.Fragment>
    );
}

ApplicationContainer.propTypes = {
    children:PropTypes.node
}

export default ApplicationContainer;