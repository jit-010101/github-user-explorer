import React from 'react';

import { makeStyles, ThemeProvider } from '@material-ui/core/styles';

import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import GitHubIcon from '@material-ui/icons/GitHub';

import { Avatar } from '@material-ui/core';

import { DefaultTheme } from "./UIThemes";

import Search from "./Search";

import {currentUser, showUserDetails} from "../states/ApplicationState";
import { useRecoilValue, useSetRecoilState } from "recoil";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        position:"relative",
    },
    avatar: {
        width: theme.spacing(5),
        height: theme.spacing(5),
        "background-color":"transparent",
        color:"#000",
        transition:"all .2s ease-in-out",
        '&:hover': {
            transform:"rotate(45deg)"
        },
        '.active':{
            transform:"rotate(45deg)"
        }
    },
    icon: {
        width:"100% !important",
        height:"100% !important"
    },
    showButton: {
        borderRadius:'25px'
    },
    topBar: {
        position: 'relative',
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        '&> *': {
            "margin-right":theme.spacing(1)
        }
    },
    stupidRect: {
        'position': 'absolute',
        'right': '0',
        'bottom': '0',
        'background': 'red',
        'width': '500px',
        'height': '500px',
        'z-index': '-1',
        'transform': 'rotate(45deg)'
    }
}));

const TopBar: React.FC = () => {
    const classes = useStyles();
    const currentUserPtr = useRecoilValue(currentUser);
    const setShowUserDetails = useSetRecoilState(showUserDetails);
    const onClick:React.MouseEventHandler<HTMLButtonElement> = () =>  {
        if(!!currentUserPtr) {
            setShowUserDetails(true);
        }
    }
    return (
        <ThemeProvider theme={DefaultTheme}>
            <Paper className={classes.topBar}>
                <Avatar className={classes.avatar} src={currentUserPtr?.avatar_url}>
                    <GitHubIcon className={classes.icon} />
                </Avatar>
                <Search></Search>
                <Button variant="contained" color="primary" className={classes.showButton} onClick={onClick}>Show</Button>
                <div className={classes.stupidRect}></div>
            </Paper>
        </ThemeProvider>
    );
}

export default TopBar;