
import React from "react";

import InputBase from '@material-ui/core/InputBase';
import { makeStyles } from '@material-ui/core/styles';

import { ChangeEvent, ChangeEventHandler } from 'react';

import { useSetRecoilState } from "recoil";
import { currentSearchValue, showUserDetails } from "../states/ApplicationState";
import { DebounceInput } from 'react-debounce-input';

const useStyles = makeStyles((theme) => ({
    searchInput: {
        width:"100%",
        '&::after': {
            content:'""',
            display:"block",
            "border-top":"2px dotted #999",
            "position":"absolute",
            "width":"0%",
            "bottom":"0px",
            "left":"0px",
            "transition":"width .3s ease-out"
        },
        '&:hover': {
            '&::after': {
                width:"100%"
            }
        }
    }
}));

type SearchParams = {
    onKeyPress?:React.KeyboardEventHandler;
}

const Search:React.FC<SearchParams> = (params) => {
    const classes = useStyles();
    const setCurrentSearchValueState = useSetRecoilState(currentSearchValue);
    const setShowUserDetails = useSetRecoilState(showUserDetails);

    const onChange:ChangeEventHandler<HTMLInputElement> = (e) =>  {
            setCurrentSearchValueState(e.target.value);
            setShowUserDetails(false);
            console.log("current search value:", e.target.value);
        }

    const onKeyPress:React.KeyboardEventHandler<HTMLInputElement> = (e) => {
        if(e.key == "Enter") {
            setShowUserDetails(true);
        }
    } 

    return (
        <InputBase 
        placeholder="Github Username ..."
        inputProps={{style: {fontSize: "large"}, debounceTimeout:"920"}}
        className={classes.searchInput}
        onChange={onChange}
        onKeyPress={onKeyPress}
        inputComponent={DebounceInput}
        type="search"/>
    );
}

export default Search;