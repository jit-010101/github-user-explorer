import React, { useEffect, useRef } from "react";

import { useRecoilValue } from 'recoil';
import { currentUser } from "../states/ApplicationState";

import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid, { GridSize } from '@material-ui/core/Grid';
import { Card, CardContent, CardMedia, Typography } from "@material-ui/core";
import GitHubIcon from '@material-ui/icons/GitHub';

import { useQuery } from 'react-query';

import { animate, motion } from "framer-motion";
import { IGist } from "src/types/Gist";
import { QUERY_CACHE_TIME_MS } from "src/constants";
import { APIConsumerService } from "src/services/APIConsumerService";
import { IOrganization } from "src/types/Organization";
import { IRepository } from "src/types/Repository";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
        height: "100%"
    },
    content: {
        "padding-bottom": "0 !important"
    },
    avatarContainer: {
        padding: theme.spacing(0),
        textAlign: 'center',
        color: theme.palette.text.secondary,
        height: "100%"
    },
    avatar: {
        height: "200px",
        width: "auto"
    },
    overview: {
        "text-transform": "uppercase",
        margin: theme.spacing(0.5)
    },
    userSince: {
        display: 'inline-flex',
        VerticalAlign: 'text-bottom',
        BoxSizing: 'inherit',
        textAlign: 'center',
        AlignItems: 'center'
    }
}));

type RndGridItemProps = {
    children?: React.ReactNode;
    minSize?: number;
    maxSize?: number;
}

const RndGridItem: React.FC<RndGridItemProps> = (props) => {
    const minSize = props.minSize || 3;
    const maxSize = props.maxSize || 6;
    const randSize: GridSize = minSize + Math.floor(Math.random() * (maxSize - minSize)) as GridSize;
    const hoverParams = {
        scale: 1.3,
        rotate: -(Math.random() * 5),
        transition: { duration: 0.3, ease: "easeOut" }
    }
    return (
        <Grid item xs={randSize}
            component={motion.div}
            whileHover={hoverParams}
            layout
        >
            {props.children}
        </Grid>
    );
}

type GridItemProps = {
    children?: React.ReactNode;
    minSize?: number;
    maxSize?: number;
}

const GridItem: React.FC<GridItemProps> = (props) => {
    const hoverParams = {
        scale: 1.3,
        rotate: -(Math.random() * 5),
        transition: { duration: 0.3, ease: "easeOut" }
    }
    return (
        <Grid item xs
            component={motion.div}
            layout
        >
            {props.children}
        </Grid>
    );
}
type CounterProps = {
    from:number;
    to:number;
}
const Counter: React.FC<CounterProps> = (props) => {
    const nodeRef = useRef<HTMLParagraphElement>(null);
    useEffect(() => {
        const node = nodeRef.current;

        const controls = animate(props.from, props.to, {
            duration: 1,
            onUpdate(value) {
                node!.textContent = value.toFixed(2);
            }
        });

        return () => controls.stop();
    }, [props.from, props.to+1]);

    return <p ref={nodeRef} />;
}

const UserDataPanes: React.FC = () => {
    const classes = useStyles();
    const currentUserPtr = useRecoilValue(currentUser);
    const currentUserName = currentUserPtr?.login!;
    const gists = useQuery<IGist[], Error>(
        ["gists", currentUserName],
        async () => {
            try {
                //const data:IGist[] = [];
                //return Promise.resolve(data);
                return await APIConsumerService.getUserGists(currentUserName);
            } catch (e) {
                return Promise.reject(e);
            }
        },
        { cacheTime: QUERY_CACHE_TIME_MS * 5 }
    );
    const repos = useQuery<IRepository[], Error>(
        ["repos", currentUserName],
        async () => {
            try {
                //const data:IRepository[] = [];
                //return Promise.resolve(data);
                return await APIConsumerService.getUserRepos(currentUserName);
            } catch (e) {
                return Promise.reject(e);
            }
        },
        { cacheTime: QUERY_CACHE_TIME_MS * 5 }
    );
    const orgs = useQuery<IOrganization[], Error>(
        ["orgs", currentUserName],
        async () => {
            try {
                // const data:IOrganization[] = [];
                // return Promise.resolve(data);
                return await APIConsumerService.getUserOrganizations(currentUserName);
            } catch (e) {
                return Promise.reject(e);
            }
        },
        { cacheTime: QUERY_CACHE_TIME_MS * 5 }
    );
    const gistsLen:number = (gists && gists.data && gists.data.length > 0)?gists!.data!.length+1:0;
    const orgsLen:number = (orgs && orgs.data && orgs.data.length > 0)?orgs!.data!.length+1:0;
    const reposLen:number = (repos && repos.data && repos.data.length > 0)?repos!.data!.length+1:0;
    return (
        <motion.div className={classes.root} layoutId="grid">
            <Grid container spacing={3}>
                <RndGridItem minSize={3} maxSize={6} >
                    <Card className={classes.avatarContainer}>
                        <CardMedia
                            image={currentUserPtr?.avatar_url}
                            title={currentUserPtr?.login}
                            className={classes.avatar}
                        />
                        <CardContent className={classes.content}>
                            <Typography component="h4" variant="h4">
                                {currentUserPtr?.login}
                            </Typography>
                            <Typography variant="subtitle1" color="textSecondary" className={classes.userSince}>
                                <GitHubIcon></GitHubIcon>-User since {new Date(currentUserPtr!.created_at!).toLocaleDateString()}
                            </Typography>
                            <Grid container spacing={1} className={classes.overview}>
                                <Grid item xs>
                                    <Typography variant="subtitle2" color="textSecondary">
                                        Gists: <Counter from={0} to={gistsLen}/>
                                    </Typography>
                                </Grid>
                                <Grid item xs>
                                    <Typography variant="subtitle2" color="textSecondary">
                                        Orgs: <Counter from={0} to={orgsLen}/>
                                    </Typography>
                                </Grid>
                                <Grid item xs>
                                    <Typography variant="subtitle2" color="textSecondary">
                                        Repos: <Counter from={0} to={reposLen}/>
                                    </Typography>
                                </Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                </RndGridItem>
                <GridItem>
                    <Paper className={classes.paper}>{currentUserPtr?.email}</Paper>
                </GridItem>
            </Grid>
            <Grid container spacing={3}>
                <GridItem>
                    <Paper className={classes.paper}>xs</Paper>
                </GridItem>
                <GridItem>
                    <Paper className={classes.paper}>xs=6</Paper>
                </GridItem>
                <GridItem>
                    <Paper className={classes.paper}>xs</Paper>
                </GridItem>
            </Grid>
        </motion.div>
    );
}

export default UserDataPanes;
