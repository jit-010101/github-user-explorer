import React from "react";

import Box from "@material-ui/core/Box";
import ButtonBase from "@material-ui/core/ButtonBase";
import { makeStyles } from "@material-ui/core/styles";

import PropTypes from "prop-types";

const useStyles = makeStyles((theme) => ({
    background: {
        display:"flex",
        position:"absolute",
        "z-index":"1",
        width:"100%",
        height:"100%"
    },
    contentArea: {
        "z-index":"2",
        position:"relative",
        padding:theme.spacing(3)
    }
}));

const ContentArea: React.FC = ({ children }) => {
    const classes = useStyles();
    return (
        <React.Fragment>
            <ButtonBase className={classes.background}></ButtonBase>
            <Box className={classes.contentArea}>
                {children}
            </Box>
        </React.Fragment>
    );
}

ContentArea.propTypes = {
    children:PropTypes.node
}

export default ContentArea;
