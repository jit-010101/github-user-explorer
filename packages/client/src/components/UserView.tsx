import React, { useRef, useEffect } from "react";

import { useQuery } from 'react-query';
import { useRecoilState, useRecoilValue, useSetRecoilState } from 'recoil';

import { makeStyles } from '@material-ui/core/styles';

import { QUERY_CACHE_TIME_MS } from "../constants";
import { IUserData } from '../types/User';

import { currentSearchValue, currentUser, showUserDetails } from "../states/ApplicationState";
import { APIConsumerService } from "../services/APIConsumerService";

import { motion } from "framer-motion";

import UserDataPanes from "./UserDataPanes";

const useStyles = makeStyles((theme) => ({

}));

const UserView: React.FC = () => {
    const classes = useStyles();
    const [searchValueState, setSearchValueState] = useRecoilState(currentSearchValue);
    const showUserDetailsState = useRecoilValue(showUserDetails);
    const setCurrentUser = useSetRecoilState(currentUser);
    
    const latestSearchValueState = useRef(searchValueState);
    
    useEffect(()=> {
        latestSearchValueState.current = searchValueState;
    },[searchValueState]);

    const result = useQuery<IUserData, Error>(
        ["userName", latestSearchValueState.current],
        async() => {
            try {
                // const data = {
                //     "login": "thiscantbeserious",
                //     "id": 34506214,
                //     "node_id": "MDQ6VXNlcjM0NTA2MjE0",
                //     "avatar_url": "https://avatars.githubusercontent.com/u/34506214?v=4",
                //     "gravatar_id": "",
                //     "url": "https://api.github.com/users/thiscantbeserious",
                //     "html_url": "https://github.com/thiscantbeserious",
                //     "followers_url": "https://api.github.com/users/thiscantbeserious/followers",
                //     "following_url": "https://api.github.com/users/thiscantbeserious/following{/other_user}",
                //     "gists_url": "https://api.github.com/users/thiscantbeserious/gists{/gist_id}",
                //     "starred_url": "https://api.github.com/users/thiscantbeserious/starred{/owner}{/repo}",
                //     "subscriptions_url": "https://api.github.com/users/thiscantbeserious/subscriptions",
                //     "organizations_url": "https://api.github.com/users/thiscantbeserious/orgs",
                //     "repos_url": "https://api.github.com/users/thiscantbeserious/repos",
                //     "events_url": "https://api.github.com/users/thiscantbeserious/events{/privacy}",
                //     "received_events_url": "https://api.github.com/users/thiscantbeserious/received_events",
                //     "type": "User",
                //     "site_admin": false,
                //     "name": null,
                //     "company": null,
                //     "blog": "",
                //     "location": null,
                //     "email": null,
                //     "hireable": null,
                //     "bio": null,
                //     "twitter_username": null,
                //     "public_repos": 13,
                //     "public_gists": 0,
                //     "followers": 1,
                //     "following": 0,
                //     "created_at": "2017-12-13T09:36:28Z",
                //     "updated_at": "2021-05-02T19:55:43Z"
                // }
                // setCurrentUser(data);
                // return Promise.resolve(data);
                const data = await APIConsumerService.getUserData(latestSearchValueState.current);
                if(data.login) {
                    setCurrentUser(data);
                    console.log("current user set to:", data);
                    return data;
                }
                return Promise.reject(data.message);
            } catch (e) {
                //setCurrentUser(null);
                return Promise.reject(e);
            }
        },
        { cacheTime: QUERY_CACHE_TIME_MS }
    );


    
    const data = result.data;

    const showDetails:boolean = (showUserDetailsState && !!data);

    return (
        <div>
            { showDetails &&
            <motion.div
                initial={{ x: 1000 }}
                animate={{ x: 0 }}
                exit={{ x: 0 }}>
            
                <UserDataPanes/>
            </motion.div>
            }
        </div>
        
    );
}

export default UserView;