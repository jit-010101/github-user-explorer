import { createMuiTheme } from "@material-ui/core/styles";

export const DefaultTheme = createMuiTheme({
    overrides: {
      MuiCssBaseline: {
        '@global': {
          html: {
            WebkitFontSmoothing: 'auto',
            'height':'100%',
            'width':'100%',
            overflow:'hidden'
          },
          body: {
            'height':'100%',
            'width':'100%',
            overflow:'hidden'
          }
        },
      },
    },
  });