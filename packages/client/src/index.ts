/**
 * DON'T MODIFY OR MOVE THIS UNLESS YOU KNOW WHAT YOU'RE DOING.
 * 
 * This is the Node-Module responsible for loading our App in Express.js
 * 
 * (Also used for the Dev-Server).
 * 
 * @TODO handle require gracefully in Server if this wasn't found
 */

import next from 'next';
import { resolve, sep } from 'path';

const dev = process.env.NODE_ENV !== 'production';

let nextDir = __dirname;
if (nextDir.indexOf(sep+'dist') !== -1) {
  nextDir = resolve(__dirname, '../');
}

const app = next({
  dev,
  dir: nextDir
});

export default app;