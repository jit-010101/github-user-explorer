import { IGist } from "../types/Gist";
import { IOrganization } from "../types/Organization";
import { IRepository } from "../types/Repository";
import { IUserData, UserDTO } from "../types/User";

function buildPath(paths: string[] = []): string {
    return paths.join('/');
}
export class APIConsumerService {
    static get API_BASE_URL(): string {
        return "https://api.github.com";
    }
    /**
     * Our generic typed fetch for the API
     * @param fromPath {string}
     * @param init {RequestInit} 
     * @returns {Response}
     */
    static fetchData<T>(fromPath: string, init?: RequestInit): Promise<T> {
        return fetch(
            buildPath([this.API_BASE_URL, fromPath]),
            init
        ).then(response => {
            return response.json() as Promise<T>;
        });
    }
    /**
     * Gets the UserData for the {@param userName}
     * @param userName {string}
     * @returns IUserData
     */
    static async getUserData(userName: string): Promise<IUserData> {
        return APIConsumerService.fetchData(
            buildPath(["users", userName])
        );
    }
    /**
     * Gets the gists from a user
     * @param userName {string}
     * @returns IGist[]
     */
    static async getUserGists(userName: string): Promise<IGist[]> {
        return APIConsumerService.fetchData(
            buildPath(["users", userName, "gists"])
        );
    }
    /**
     * Gets the repos from a user
     * @param userName {string}
     * @returns IRepository[]
     */
    static async getUserRepos(userName: string): Promise<IRepository[]>{
        return APIConsumerService.fetchData(
            buildPath(["users", userName, "repos"])
        );
    }
    /**
     * Gets the organizations from a user
     * @param userName {string}
     * @returns IOrganization[]
     */
    static async getUserOrganizations(userName: string): Promise<IOrganization[]> {
        return APIConsumerService.fetchData(
            buildPath(["users", userName, "orgs"])
        );
    }
}
