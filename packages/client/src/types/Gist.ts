import { DTO } from "./DTO";

export interface IGistFile {
    filename?:string,
    type?:string,
    language?:string,
    size?:number,
    raw_url?:string
}
export interface IGist {
    description?:string,
    created_at?:string,
    updated_at?:string
    files?:IGistFile[]
}

export class GistDTO 
    extends DTO 
    implements IGist {
    description?:string;
    created_at?:string;
    updated_at?:string;
    files?:IGistFile[];
}