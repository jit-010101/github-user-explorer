import { DTO } from "./DTO";

export interface IRepository {
    id?:number,
    name?:string,
    clone_url?:string,
    stargazers_count?:number,
    forks_count?:number,
    forks_url?:string,
    watchers?:number,
}

export class RepositoryDTO
    extends DTO
    implements IRepository {
    id?:number;
    name?:string;
    clone_url?:string;
    stargazers_count?:number;
    forks_count?:number;
    forks_url?:string;
    watchers?:number;
}