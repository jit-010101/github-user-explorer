export interface IDTO {
    fill<T>(other:Partial<T>):void;
}

export abstract class DTO {
    fill<T>(other:Partial<T>):void {
        Object.assign(this, other);
    }
}