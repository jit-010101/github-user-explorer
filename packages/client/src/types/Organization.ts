import { DTO } from "./DTO"

export interface IOrganization {
    id?:number
}

export class OrganizationDTO 
    extends DTO
    implements IOrganization {
    id?:number;
}
