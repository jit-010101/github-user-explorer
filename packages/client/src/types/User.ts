import { IGist } from "./Gist";
import { IOrganization } from "./Organization";
import { IRepository } from "./Repository";

import { DTO } from "./DTO";
export interface IUserData {
    login?: string,
    id?: number,
    name?: string,
    email?: string
    company?: string,
    avatar_url?: string,
    html_url?:string,
    gists?:IGist[],
    orgs?:IOrganization[],
    repos?:IRepository[],
    created_at?:Date,
    message?:string //would be better of in a generic interface, but to lazy now
}
export class UserDTO
    extends DTO
    implements IUserData {
    login?: string;
    id?: number;
    name?: string;
    email?: string;
    company?: string;
    avatar_url?: string;
    html_url?:string;
    gists?:IGist[];
    orgs?:IOrganization[];
    repos?:IRepository[]
}