import { atom } from "recoil";
import { IUserData } from "../types/User";

/** 
 * Reminder: atom-keys need to be unique in each store!
 */

/**
 * Stores the value of our search input to be shared accross the Application.
 */1
export const currentSearchValue = atom<string>({
    key:"currentSearchValue",
    default:""
});

/**
 * Show user details
 */
 export const showUserDetails = atom<boolean>({
    key:"showUserDetails",
    default:false
});

/**
 * Store the current user to be shared accross the Application.
 */
export const currentUser = atom<IUserData|null|undefined>({
    key:"currentUser",
    default:undefined
});