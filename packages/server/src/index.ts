import express from 'express';
import cookiesMiddleware from 'universal-cookie-express';
import bodyParser from 'body-parser';
import compression from 'compression';
import cookieParser from 'cookie-parser';

import client from '@github-user-explorer/client';

import * as routes from './routes';

const port = process.env.PORT ? parseInt(process.env.PORT, 10) : 3000;

const server = express();
        server.set('x-powered-by', false);
        server.use(compression());
        server.use(bodyParser.json());
        server.use(bodyParser.urlencoded({ extended: true }));
        server.use(cookieParser());
        server.use(cookiesMiddleware());
        server.use('/api', routes.serverAPIRouter);
        server.use('/', routes.nextJSRouter);

//@TODO logging for deployment
client.prepare()
  .then(()=>server.listen(port))
  .catch((error)=>console.error(error));
