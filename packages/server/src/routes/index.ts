import nextJSRouter from './nextJS';
import serverAPIRouter from './api';

export { 
    nextJSRouter, 
    serverAPIRouter 
};