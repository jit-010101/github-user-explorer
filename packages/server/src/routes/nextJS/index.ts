import { Router, Request, Response } from 'express';
import URL from 'url';
import client from '@github-user-explorer/client';

const router = Router();
const handler = client.getRequestHandler();

// TODO: FIX deprecation warning. (NextJS requires the query string parameter)
router.get('/*', (req: Request, res: Response) => {
  // eslint-disable-next-line
  return handler(req, res, { ...URL.parse(req.url, true) });
});

export default router;