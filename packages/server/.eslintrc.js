module.exports = {
    "ignorePatterns": [
        "dist/**/*",
        "src/utils/**/*",
        "**/*.test.(js|jsx|tsx|ts)",  
        "**/*.config.(js|json|ts)"
    ],
    "env": {
        "es2021": true,
        "node": true
    },
    "extends": [
        "eslint:recommended",
        "plugin:@typescript-eslint/recommended"
    ],
    "parser": "@typescript-eslint/parser",
    "parserOptions": {
        "ecmaVersion": 12,
        "sourceType": "module"
    },
    "plugins": [
        "@typescript-eslint"
    ],
    "rules": {
    }
};
